﻿@students =
    EXTRACT Student string,
            Subject string,
            Exam string,
            EventDate DateTime,
            MaxPoints int,
            PointsEarned int
    FROM "/DataLakeTutorial/students.csv"
    USING Extractors.Csv(skipFirstNRows : 1);

@students =
    SELECT Subject,
           Exam,
           AVG((float) PointsEarned / (float) MaxPoints) AS AverageRanking
    FROM @students
    GROUP BY Subject,
             Exam;

/*  The MAP_AGG aggregator takes a key, value pair 
    and creates a SQL.MAP for all the key/value pairs in the group. 
*/

@students =
    SELECT Subject,
           MAP_AGG(Exam, AverageRanking.ToString()) AS MappedExams
    FROM @students
    GROUP BY Subject;

/*  MAP_AGG and EXPLODE are inverse operations. 
*/

@students =
    SELECT Subject,
           exams.exam AS Exam,
           exams.averageRanking AS AverageRanking
    FROM @students
         CROSS APPLY
             EXPLODE(MappedExams) AS exams(exam, averageRanking);

OUTPUT @students
TO "/DataLakeTutorial/10_EXPLODE_students.tsv"
USING Outputters.Tsv();
